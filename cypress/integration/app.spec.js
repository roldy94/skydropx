/* eslint-disable no-undef */

describe("Navigation", () => {
  it("should navigate to the shipment page", () => {
    // Start from the index page
    cy.visit("http://localhost:3000/");

    cy.get('input[name="zipFrom"]').type("02900").should("have.value", "02900");
    cy.get('input[name="zipTo"]').type("44100").should("have.value", "44100");
    cy.get('input[name="weight"]').type("1").should("have.value", "1");
    cy.get('input[name="height"]').type("10").should("have.value", "10");
    cy.get('input[name="width"]').type("10").should("have.value", "10");
    cy.get('input[name="length"]').type("10").should("have.value", "10");
    // Find a link with an href attribute containing "about" and click it
    cy.get("button").click();

    // The new url should include "/shipment"
    cy.location("pathname", { timeout: 60000 }).should("include", "/shipment");

    // The new page should contain an th
    cy.get("table").contains("th", "ZIP FROM");
  });
});
