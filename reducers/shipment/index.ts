import { HomeAction } from './../home/actions';
import { LabelAction } from './../label/actions';
import { ShipAction, Action } from './actions';

export type StateShip = {
  shipment: any;
};
export const initialStateShip = {
  shipment: {},
};
const reducer = (
  state: StateShip,
  action: ShipAction | HomeAction | LabelAction
) => {
  switch (action.type) {
    case Action.REQUEST_SHIP:
      return {
        ...state,
        shipment: action.payload,
      };
    default:
      return state;
  }
};
export default reducer;
