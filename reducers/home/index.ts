import { HomeAction, Action } from './actions';
import { ShipAction } from '../shipment/actions';
import { LabelAction } from '../label/actions';

export type StateHome = {
  home: any[];
};
export const initialStateHome = {
  home: [],
};
const reducer = (
  state: StateHome,
  action: HomeAction | ShipAction | LabelAction
) => {
  switch (action.type) {
    case Action.REQUEST_HOME:
      return {
        ...state,
        home: state.home.concat(action.payload),
      };
    default:
      return state;
  }
};
export default reducer;
