export enum Action {
  REQUEST_HOME = 'request_home',
}
export interface HomeAction {
  type: Action;
  payload: any;
}
