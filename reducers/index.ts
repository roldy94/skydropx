import homeReducer from './home';
import shipmentReducer from './shipment';
import labelReducer from './label';

export { homeReducer, shipmentReducer, labelReducer };
