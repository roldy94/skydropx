import { HomeAction } from '../home/actions';
import { ShipAction } from '../shipment/actions';
import { LabelAction, Action } from './actions';

export type StateLabel = {
  label: any;
};
export const initialStateLabel = {
  label: {},
};
const reducer = (
  state: StateLabel,
  action: LabelAction | HomeAction | ShipAction
) => {
  switch (action.type) {
    case Action.REQUEST_LABEL:
      return {
        ...state,
        label: action.payload,
      };
    default:
      return state;
  }
};
export default reducer;
