import React, { createContext, useReducer } from 'react';
import { homeReducer, shipmentReducer, labelReducer } from './index';
import { StateHome, initialStateHome } from './home';
import { StateShip, initialStateShip } from './shipment';
import { StateLabel, initialStateLabel } from './label';
import { HomeAction } from './home/actions';
import { ShipAction } from './shipment/actions';
import { LabelAction } from './label/actions';

type InitialStateType = {
  home: StateHome;
  shipment: StateShip;
  label: StateLabel;
};
const initialState = {
  home: initialStateHome,
  shipment: initialStateShip,
  label: initialStateLabel,
};

const AppContext = createContext<{
  state: InitialStateType;
  dispatch: React.Dispatch<HomeAction | ShipAction | LabelAction>;
}>({
  state: initialState,
  dispatch: () => null,
});

const mainReducer = (
  { home, shipment, label }: InitialStateType,
  action: HomeAction | ShipAction | LabelAction
) => ({
  home: homeReducer(home, action),
  shipment: shipmentReducer(shipment, action),
  label: labelReducer(label, action),
});

const AppProvider = (props: any) => {
  const [state, dispatch] = useReducer(mainReducer, initialState);
  return (
    <AppContext.Provider value={{ state, dispatch }}>
      {props.children}
    </AppContext.Provider>
  );
};

export { AppContext, AppProvider };
