# Skydropx for roldy94

This is just a challenge

## Description

A simple page that contains a form to then display the results in a table

![presentacion](./assets/presentacion.gif)

## Demo live

[demo live](https://skydropx-roldy94.vercel.app/)

## Getting Started

### Dependencies

- node lasted version

### Installing

```
    npm install
```

### Executing program

```
    npm run dev
```

### Testing

```
    npm run test
```

### Testing E2E

```
    npm run cypress
```

## Feature

### Sorting

![sorting](./assets/sorting.gif)

### Filter

![filter](./assets/filter.gif)

### Responsive Desing

![mobile](./assets/mobile.gif)

## Authors

Contributors names and contact info
ex. [@roldy94](https://gitlab.com/roldy94)

## Version History

- 0.1
  - Initial Release

## License

This project is licensed under the [NAME HERE] License - see the LICENSE.md file for details

## Acknowledgments

- [React Table + Tailwind CSS](https://github.com/jimmybutton/react-tailwind-table/tree/part1)
- [midudev](https://github.com/midudev)
- [next js](https://nextjs.org/docs/getting-started)
- [Tailwind CSS](https://tailwindcss.com/docs/installation)
