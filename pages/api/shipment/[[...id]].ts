import type { NextApiRequest, NextApiResponse } from 'next';
import { fetcher } from '../../../utils';

const url = 'shipments';
const METHOD = {
  post: 'POST',
  get: 'GET',
};
const BODY_MOCK = (zipFrom: string, zipTo: string, parcel: any) => {
  return {
    address_from: {
      province: 'Ciudad de México',
      city: 'Azcapotzalco',
      name: 'Jose Fernando',
      zip: zipFrom,
      country: 'MX',
      address1: 'Av. Principal #234',
      company: 'skydropx',
      address2: 'Centro',
      phone: '5555555555',
      email: 'skydropx@email.com',
    },
    parcels: [parcel],
    address_to: {
      province: 'Jalisco',
      city: 'Guadalajara',
      name: 'Jorge Fernández',
      zip: zipTo,
      country: 'MX',
      address1: ' Av. Lázaro Cárdenas #234',
      company: '-',
      address2: 'Americana',
      phone: '5555555555',
      email: 'ejemplo@skydropx.com',
      reference: 'Frente a tienda de abarro',
      contents: 'test',
    },
    consignment_note_class_code: '53131600',
    consignment_note_packaging_code: '1H1',
  };
};

async function post(req: NextApiRequest, res: NextApiResponse<any>) {
  const { body } = req;
  const parseBody = JSON.parse(body);
  const responses = await fetcher(
    url,
    METHOD.post,
    JSON.stringify(
      BODY_MOCK(parseBody.zipFrom, parseBody.zipTo, parseBody.parcel)
    )
  );
  const data = await responses.json();
  res.json(data);
}

async function get(url: string, res: NextApiResponse<any>) {
  const responses = await fetcher(url, METHOD.get);
  const data = await responses.json();
  res.json(data);
}

export default async function (req: NextApiRequest, res: NextApiResponse<any>) {
  const { query } = req;
  const { id } = query;
  if (req.method === METHOD.post) return post(req, res);
  return id ? get(`${url}/${id}`, res) : get(`${url}/`, res);
}
