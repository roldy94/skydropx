import type { NextApiRequest, NextApiResponse } from 'next';
import { fetcher } from '../../../utils';

const url = 'labels';
const METHOD = {
  post: 'POST',
  get: 'GET',
};

async function post(req: NextApiRequest, res: NextApiResponse<any>) {
  const { body } = req;
  const parseBody = JSON.parse(body);
  const responses = await fetcher(
    url,
    METHOD.post,
    JSON.stringify({
      rate_id: Number(parseBody.rateId),
      label_format: parseBody.labelFormat,
    })
  );
  const data = await responses.json();
  res.json(data);
}

export default async function (req: NextApiRequest, res: NextApiResponse<any>) {
  if (req.method === METHOD.post) return post(req, res);
}
