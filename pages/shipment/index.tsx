import React, { useContext, useState } from 'react';
import type { NextPage } from 'next';
import {
  Main,
  Table,
  SelectColumnFilter,
  Modal,
  Spinner,
} from '../../components';
import { AppContext } from '../../reducers/context';
import { useLabel } from '../../hooks';
import absoluteUrl from 'next-absolute-url';

const Shipment: NextPage = (props: any) => {
  const { state } = useContext(AppContext);
  const { createLabel, isLoading } = useLabel();
  const [showModal, setShowModal] = useState(false);

  const handleClick = (row: any) => {
    setShowModal(true);
    createLabel(row.id, 'pdf');
  };

  const columns = [
    {
      Header: 'AMOUNT LOCAL',
      accessor: 'attributes.amount_local',
    },
    {
      Header: 'CURRENCY LOCAL',
      accessor: 'attributes.currency_local',
    },
    {
      Header: 'CREATED AT',
      accessor: 'attributes.created_at',
    },
    {
      Header: 'DAYS',
      accessor: 'attributes.days',
    },
    {
      Header: 'OUT OF AREA PRICING',
      accessor: 'attributes.out_of_area_pricing',
    },
    {
      Header: 'PROVIDER',
      accessor: 'attributes.provider',
      Filter: SelectColumnFilter,
      filter: 'includes',
    },
    {
      Header: 'SERVICE',
      accessor: 'attributes.service_level_name',
      Filter: SelectColumnFilter,
      filter: 'includes',
    },
    {
      Header: 'TOTAL',
      accessor: 'attributes.total_pricing',
    },
  ];

  return (
    <Main>
      <div className='w-full p-4 md:p-14 lg:p-0 lg:w-auto mt-10 sm:mt-0'>
        <Table
          columns={columns}
          data={props.shipment}
          handleClick={handleClick}
        />
        <Modal
          showModal={showModal}
          onClick={() => (showModal ? setShowModal(false) : setShowModal(true))}
          title={'Label Information'}
        >
          {isLoading ? (
            <div className='flex justify-center items-center '>
              <Spinner main={false} />
            </div>
          ) : state.label?.label?.attributes?.status === 'ERROR' ? (
            <div className='flex justify-center items-center '>
              <p>Error</p>
            </div>
          ) : (
            <iframe
              className='w-full h-full'
              src={state.label?.label?.attributes?.label_url}
            />
          )}
        </Modal>
      </div>
    </Main>
  );
};

export async function getServerSideProps(context: any) {
  const { query, req } = context;
  const { protocol, host } = absoluteUrl(req);
  const apiURL = `${protocol}//${host}/api/shipment`;
  const res = await fetch(apiURL, {
    method: 'POST',
    body: JSON.stringify({
      zipFrom: query.zipFrom,
      zipTo: query.zipTo,
      parcel: {
        weight: Number(query.weight),
        distance_unit: 'CM',
        mass_unit: 'KG',
        height: Number(query.height),
        width: Number(query.width),
        length: Number(query.length),
      },
    }),
  });
  const data = await res.json();
  const shipment = data.included.filter((item: any) => item.type === 'rates');
  // Pass data to the page via props
  return { props: { shipment } };
}

export default React.memo(Shipment);
