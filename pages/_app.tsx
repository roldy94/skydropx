/* eslint-disable react/prop-types */
import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { AppProvider } from '../reducers/context';
import { Main } from '../components';
import { ErrorBoundary } from 'react-error-boundary';

function ErrorFallback({ error, resetErrorBoundary }: any) {
  return (
    <Main>
      <h1 className='text-8xl font-bold'>Something went wrong:</h1>
      <h1 className='text-8xl font-bold'>;(</h1>
      <h2 className='mt-8 text-lg md:text-2xl'>{error.message}</h2>
      <button onClick={resetErrorBoundary}>Try again</button>
    </Main>
  );
}

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ErrorBoundary
      FallbackComponent={ErrorFallback}
      onReset={() => {
        // reset the state of your app so the error doesn't happen again
      }}
    >
      <AppProvider>
        <Component {...pageProps} />
      </AppProvider>
    </ErrorBoundary>
  );
}

export default MyApp;
