import Link from 'next/link';
import { Main } from '../components';

export default function NotFound() {
  return (
    <Main>
      <h1 className='text-8xl font-bold'>404</h1>
      <h1 className='text-8xl font-bold'>;(</h1>
      <h2 className='mt-8 text-lg md:text-2xl'>
        Ooooops! That page cannot be found
      </h2>
      <p className='text-lg md:text-2xl'>
        Go bach the home {''}
        <Link href='/'>
          <a className='text-cyan-500'>Homepage</a>
        </Link>
      </p>
    </Main>
  );
}
