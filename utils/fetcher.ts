const API_URL = `${process.env.BASE_URL}`;
const headers = new Headers({
  Authorization: `Token token=${process.env.API_KEY}`,
  'Content-Type': 'application/json',
});

const fetcher = async (
  url: string,
  method: string,
  body?: BodyInit,
  absoluteUrl?: string
) => {
  const newUrl = absoluteUrl || `${API_URL}/${url}`;
  const responses = fetch(newUrl, {
    method,
    headers,
    body,
  });
  return await responses;
};

export default fetcher;
