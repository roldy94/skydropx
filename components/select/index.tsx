/* eslint-disable react/prop-types */
import React from 'react';

export default function SelectColumnFilter({
  column: { filterValue, setFilter, preFilteredRows, id, render },
}: any) {
  const options = React.useMemo(() => {
    const options = new Set();
    preFilteredRows.forEach((row: any) => {
      options.add(row.values[id]);
    });
    return [...(options.values() as any)];
  }, [id, preFilteredRows]);
  let header: string = render('Header');
  header = header.charAt(0).toUpperCase() + header.toLowerCase().slice(1);
  return (
    <label className='flex gap-x-2 items-baseline pr-4 pt-4'>
      <span className='text-gray-700'>{header}: </span>
      <select
        className='w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50'
        name={id}
        id={id}
        value={filterValue}
        onChange={(e) => {
          setFilter(e.target.value || undefined);
        }}
      >
        <option value=''>All</option>
        {options.map((option, i) => (
          <option key={i} value={option}>
            {option}
          </option>
        ))}
      </select>
    </label>
  );
}
