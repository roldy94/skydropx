/* eslint-disable react/jsx-key */
import {
  useFilters,
  useGlobalFilter,
  usePagination,
  useSortBy,
  useTable,
} from 'react-table';
import { GlobalFilter, Icon } from '../index';

export default function Table({ columns, data, handleClick }: any) {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state,
    preGlobalFilteredRows,
    setGlobalFilter,
  } = useTable(
    {
      columns,
      data,
    },
    useFilters,
    useGlobalFilter,
    useSortBy,
    usePagination
  );
  return (
    <div className='flex flex-col container md: p-8'>
      <div className='flex flex-col pb-4 lg:flex-row'>
        <GlobalFilter
          preGlobalFilteredRows={preGlobalFilteredRows}
          globalFilter={state.globalFilter}
          setGlobalFilter={setGlobalFilter}
        />
        {headerGroups.map((headerGroup) =>
          headerGroup.headers.map((column) =>
            column.Filter ? column.render('Filter') : null
          )
        )}
      </div>
      <div className='flex'>
        <div className='-my-2 overflow-x-auto -mx-4 sm:-mx-6 lg:-mx-8'>
          <div className='py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8'>
            <div className='shadow overflow-hidden border-b border-gray-200 sm:rounded-lg'>
              <table
                {...getTableProps()}
                className='min-w-full divide-y divide-gray-200'
              >
                <thead className='bg-gray-50'>
                  {headerGroups.map((headerGroup) => (
                    <tr {...headerGroup.getHeaderGroupProps()}>
                      {headerGroup.headers.map((column) => (
                        <th
                          scope='col'
                          className='hover:scale-105 w-full h-full transition-all duration-500 ease-in-out transform bg-center bg-cover hover:shadow-2xl  group px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider'
                          {...column.getHeaderProps(
                            column.getSortByToggleProps()
                          )}
                        >
                          <div className='flex items-center justify-between'>
                            {column.render('Header')}
                            <span>
                              {column.isSorted
                                ? column.isSortedDesc
                                  ? Icon(
                                      'SortDownIcon',
                                      'w-4 h-4 text-gray-400'
                                    )
                                  : Icon('SortUpIcon', 'w-4 h-4 text-gray-400')
                                : Icon(
                                    'SortIcon',
                                    'w-4 h-4 text-gray-400 opacity-0 group-hover:opacity-100'
                                  )}
                            </span>
                          </div>
                        </th>
                      ))}
                    </tr>
                  ))}
                </thead>
                <tbody
                  className='bg-white divide-y divide-gray-200'
                  {...getTableBodyProps()}
                >
                  {rows.map((row) => {
                    prepareRow(row as any);
                    return (
                      <tr
                        {...row.getRowProps()}
                        onClick={() => handleClick(row.original)}
                        className='hover:cursor-pointer hover:rounded-2xl hover:scale-95 w-full h-full transition-all duration-500 ease-in-out transform bg-center bg-cover hover:shadow-2xl'
                      >
                        {row.cells.map((cell) => {
                          return (
                            <td
                              className='px-6 py-4 whitespace-nowrap '
                              {...cell.getCellProps()}
                            >
                              {cell.render('Cell')}
                            </td>
                          );
                        })}
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
