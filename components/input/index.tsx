type Props = {
  label?: string;
  name: string;
};

const Input = ({ label = 'label', name = 'name' }: Props) => {
  return (
    <>
      <label
        htmlFor='first-name'
        className='block text-sm font-medium text-gray-700'
      >
        {label}
      </label>
      <input
        type='text'
        name={name}
        id='first-name'
        autoComplete='given-name'
        className='mt-1 py-2 px-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md'
      />
    </>
  );
};

export default Input;
