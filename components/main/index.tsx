import React from 'react';

interface Props {
  children: React.ReactNode;
}
const Main: React.FC<Props> = ({ children }): JSX.Element => {
  if (!Array.isArray(children))
    return (
      <div className='flex md:flex-col justify-center items-center min-h-screen'>
        {children}
      </div>
    );
  return (
    <div className='flex md:flex-col justify-center items-center min-h-screen'>
      {children.map((child, i, arr) => {
        if (i + 1 === arr.length) return child;
        return (
          <div key={i}>
            {child}
            <br />
          </div>
        );
      })}
    </div>
  );
};

export default Main;
