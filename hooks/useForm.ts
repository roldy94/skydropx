import { useContext } from 'react';
import { AppContext } from '../reducers/context';
import { Action } from '../reducers/home/actions';

export default function useForm() {
  const { dispatch } = useContext(AppContext);
  const add = (req: any) => {
    console.log(req);
    dispatch({
      type: Action.REQUEST_HOME,
      payload: req,
    });
  };
  return {
    onSubmit: add,
  };
}
