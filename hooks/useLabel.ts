import { useState, useContext } from 'react';
import { AppContext } from '../reducers/context';
import { Action } from '../reducers/label/actions';

export default function useLabel() {
  const [isLoading, setLoading] = useState(false);
  const { dispatch } = useContext(AppContext);
  const createLabel = (rateId: string, labelFormat: string) => {
    setLoading(true);
    fetch('api/label', {
      method: 'POST',
      body: JSON.stringify({
        rateId,
        labelFormat,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setLoading(false);
        if (data.code !== 'label_exists') {
          dispatch({
            type: Action.REQUEST_LABEL,
            payload: data.data ? data.data : data,
          });
        }
        console.log(data);
      })
      .catch((error) => console.log(error));
  };

  return { isLoading, createLabel };
}
