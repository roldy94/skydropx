export type onSubmitReq = {
  zipTo: string;
  zipFrom: string;
  height: number;
  length: number;
  weight: number;
  width: number;
};
