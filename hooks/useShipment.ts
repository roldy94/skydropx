import { useEffect, useState, useContext } from 'react';
import { AppContext } from '../reducers/context';
import { Action } from '../reducers/shipment/actions';

export default function useShipment(state: any) {
  const [data, setData] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const { dispatch } = useContext(AppContext);
  useEffect(() => {
    setLoading(true);
    fetch('api/shipment', {
      method: 'POST',
      body: JSON.stringify({
        zipFrom: state.home.zipFrom,
        zipTo: state.home.zipTo,
        parcel: {
          weight: Number(state.home.parcel.weight),
          distance_unit: 'CM',
          mass_unit: 'KG',
          height: Number(state.home.parcel.height),
          width: Number(state.home.parcel.width),
          length: Number(state.home.parcel.length),
        },
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setData(data);
        setLoading(false);

        const filter = data.included.filter(
          (item: any) => item.type === 'rates'
        );

        dispatch({
          type: Action.REQUEST_SHIP,
          payload: filter,
        });
      })
      .catch((error) => console.log(error));
  }, []);
  return { data, isLoading };
}
