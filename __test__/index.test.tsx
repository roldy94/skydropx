import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Home from '../pages/index';

const setup = () => render(<Home />);

describe('<Home>', () => {
  it('should renders a heading', () => {
    setup();
    const heading = screen.getByRole('heading', {
      name: 'Create a Shipment',
    });
    expect(heading).toBeInTheDocument();
  });
  it('should render text form', async () => {
    setup();
    await screen.findByText('Code zip destination');
  });
  it('should render text form', async () => {
    setup();
    await screen.findByText('Code zip destination');
  });
  it('should render text form', async () => {
    setup();
    await screen.findByText('mass unit');
  });
  it('should render text form', async () => {
    setup();
    await screen.findByText('distance unit');
  });
  it('should render text form', async () => {
    setup();
    await screen.findByText('length');
  });
});
