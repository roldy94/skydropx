module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],

  theme: {
    extend: {
      width: {
        148: "48rem",
      },
      height: {
        142: "42rem",
      },
    },
  },
  plugins: [require("@tailwindcss/forms")],
};
